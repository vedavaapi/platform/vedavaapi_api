$(function() {
    queryParams = new URLSearchParams(document.location.search.substring(1));

    function setup() {
        let setupUrl = '../accounts/v1/setup/admin'
        let dataString = $('#setup-form').serialize();
        $.ajax({
            type: 'POST',
            url: setupUrl,
            data: dataString,
            success: redirectToIndex,
            error: function(xhr, status, error) {
                $("#setup-success-status").text("setup failed.");
                // redirectToIndex();
            }
        })
    }

    function absolutePath(href) {
        let link = document.createElement("a");
        link.href = href;
        return link.href;
    }

    function getConsentUrl() {
        let consentPageUrl = 'consent.html';
        let fullConsentUrl = `consent.html?${document.location.search.substring(1)}`;
        return absolutePath(fullConsentUrl);
    }

    function redirectToIndex() {
        let fullIndexUrl = '../';
        window.location.replace(fullIndexUrl);
    }

    $("#submit-button-setup").click(setup);
})
