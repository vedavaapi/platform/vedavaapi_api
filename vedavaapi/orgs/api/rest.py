import flask_restx
from . import get_orgs_config, api


@api.route('/')
class Organizations(flask_restx.Resource):

    def get(self):
        return list(get_orgs_config().keys()), 200
